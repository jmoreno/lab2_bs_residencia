from flask import Flask, jsonify, request
import time
import serial
import serial.tools.list_ports as list_ports
from threading import Thread
import residencia
from residencia import create_tables

app = Flask(__name__)

PID_MICROBIT = 516
VID_MICROBIT = 3368
TIMEOUT = 0.1

# Look for serial port where micro:bit is connected
def find_comport(pid, vid, baud):
    ''' return a serial port '''
    ser_port = serial.Serial(timeout=TIMEOUT)
    ser_port.baudrate = baud
    ports = list(list_ports.comports())
    print('scanning ports')
    for p in ports:
        print('port: {}'.format(p))
        try:
            print('pid: {} vid: {}'.format(p.pid, p.vid))
        except AttributeError:
            continue
        if (p.pid == pid) and (p.vid == vid):
            print('found target device pid: {} vid: {} port: {}'.format(
                p.pid, p.vid, p.device))
            ser_port.port = str(p.device)
            return ser_port
    return None

@app.route("/mensaje", methods=["POST"])
def show_msg():
    ser_micro = find_comport(PID_MICROBIT, VID_MICROBIT, 115200)
    details = request.get_json()
    text = details["text"]
    ser_micro.open()
    ser_micro.write(text.encode('utf-8'))
    ser_micro.close()
    return text

def task():
    print('looking for microbit')
    ser_micro = find_comport(PID_MICROBIT, VID_MICROBIT, 115200)
    if not ser_micro:
        print('microbit not found')
        return
        
    print('opening and monitoring microbit port')
    ser_micro.open()

    count = 0
    while True:
        time.sleep (0.1)

        # Has the micro:bit sent any bytes?
        line = ser_micro.readline().decode('utf-8')
        if line:  
            print(line)
            residencia.insert_mensage(line)

    ser_micro.close()


if __name__ == "__main__":
    create_tables()
    t = Thread(target=task)
    t.start()
    app.run(host='0.0.0.0', port=5000)
    
