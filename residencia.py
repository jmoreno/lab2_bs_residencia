from flask import Flask, jsonify, request
import sqlite3
DATABASE_NAME = "residencia.db"

app = Flask(__name__)

def get_db():
    conn = sqlite3.connect(DATABASE_NAME)
    return conn


def create_tables():
    tables = [
        """CREATE TABLE IF NOT EXISTS mensages(
                id INTEGER PRIMARY KEY AUTOINCREMENT,
                name TEXT NOT NULL
            )
        """
    ]
    db = get_db()
    cursor = db.cursor()
    for table in tables:
        cursor.execute(table)
        
def insert_mensage(name):
    db = get_db()
    cursor = db.cursor()
    statement = "INSERT INTO mensages(name) VALUES (?)"
    cursor.execute(statement, [name])
    db.commit()
    return True


def update_mensage(id, name):
    db = get_db()
    cursor = db.cursor()
    statement = "UPDATE mensages SET name = ? WHERE id = ?"
    cursor.execute(statement, [name, id])
    db.commit()
    return True


def delete_mensage(id):
    db = get_db()
    cursor = db.cursor()
    statement = "DELETE FROM mensages WHERE id = ?"
    cursor.execute(statement, [id])
    db.commit()
    return True


def get_by_id(id):
    db = get_db()
    cursor = db.cursor()
    statement = "SELECT id, name FROM mensages WHERE id = ?"
    cursor.execute(statement, [name])
    return cursor.fetchone()


def get_mensages():
    db = get_db()
    cursor = db.cursor()
    query = "SELECT id, name FROM mensages"
    cursor.execute(query)
    return cursor.fetchall()
    

@app.route('/mensajes', methods=["GET"])
def get_all_mesages():
    listado = get_mensages()
    return jsonify(listado)
    
if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8000, debug=False)
    
